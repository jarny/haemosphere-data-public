# README #

This repository holds various data files required for haemosphere to function:

* dataset files: each dataset is contained within a single .h5 file (HDF5 file), and may be a public dataset or a private one. Read below for more on this. 
* users.h5: this file contains all user information (username, email, etc).
* access.log: I usually call pserve with reference to this access.log, where pyramid will write its logs.

This repository should be placed directly under the top level of haemosphere code, at the same level as development.ini and setup.py. If this repository is named "data", the current configuration values in the .ini files do not need to change (I actually have data-private/ and data-public/ repos and symlink data to either of these). If using a different name for the repo directory, update the .ini files appropriately.

### Structure of the directories and privacy ###

Haemosphere uses "sharewould" model (haemosphere/models/sharewould.py) to manage the data privacy. This model uses directory structures to make datasets public or private. The structure of the directory should look like this:

data/datasets/F0r3sT/
>PUBLIC/dmap.h5

>PRIVATE/USERS/jarny/

>PRIVATE/USERS/hilton/hiltonlab.h5

>PRIVATE/GROUPS/HiltonLab/hiltonlab.h5 (-> ../../hilton/hiltonlab.h5)

The last bit denotes a symlink, which is how access is managed at the group level. So in this example, hiltonlab.h5 is a private dataset owned by user 'hilton', hence must be placed under data/datasets/F0r3sT/PRIVATE/USERS/hilton. To enable the "HiltonLab" group to access this dataset, simply create a symlink to the dataset inside data/datasets/F0r3sT/PRIVATE/GROUPS/HiltonLab. No other changes are necessary and this change can be made without running code.

Note that public datasets simply live under data/datasets/F0r3sT/PUBLIC/.

Haemosphere does not require a restart after these changes are made.